﻿function Set-CredentialStoreCredential {
<#
    .SYNOPSIS
        Adds or updates a credential for a user. If the credential file doesn't exist, it will be created.
    
    .EXAMPLE
        Set-Credential "C:\path\to\key\file"
    
    .EXAMPLE
        Set-Credential -CredentialFile "C:\path\to\key\file" -UserName "bob"

        Adds a new credential with the user name pre-populated.
#>
    param(
        [Parameter(Position=0,Mandatory=$true)]
            [String] $CredentialsFile,
        [Parameter(Position=1,Mandatory=$false)]
            [String] $UserName = ''
    )

    $Local:credentials = Open-CredentialStore $CredentialsFile
    $serialized = $null
    $credential = $null

    Try {
        $credential = Get-Credential -UserName $UserName -Message "Enter the user's credentials." -ErrorAction Stop
    } Catch {
        Write-Output "Error: $PSItem.ToString()"
        Break
    }

    # Add new credential, or update existing.
    $Local:credentials[$credential.UserName] = ($credential.Password | ConvertFrom-SecureString)

    foreach($key in $credentials.Keys)
    {
        $serialized = "{0}{1}|{2}`n" -f $serialized, $key, $credentials[$key]
    }

    $serialized | Set-Content $CredentialsFile

}

function Get-CredentialStoreCredential {
<#
    .SYNOPSIS
        Gets a PSCredential from a credential file for a specified user.
#>
    param(
        [Parameter(Position=0,Mandatory=$true)]
            [String] $CredentialsFile,
        [Parameter(Position=1,Mandatory=$true)]
            [String] $UserName
    )

    $Local:credentials = Open-CredentialStore -CredentialsFile $CredentialsFile -UserName $UserName
    if($Local:credentials.Count -eq 0) {
        Write-Error -Message "User not found." -ErrorAction Stop
    } else {
        
        $Local:secret = ($Local:credentials[$UserName] | ConvertTo-SecureString)
        $Local:credential = New-Object System.Management.Automation.PSCredential($UserName, $Local:secret)

        return $Local:credential
    }
}

function Show-CredentialStoreHasCredential {
    param(
        [Parameter(Position=0,Mandatory=$true)]
            [String] $CredentialsFile,
        [Parameter(Position=1,Mandatory=$true)]
            [String] $UserName
    )

    $Local:credentials = Open-CredentialStore -CredentialsFile $CredentialsFile -UserName $UserName
    if($Local:credentials.Count -eq 1) { return $true }
    else { return $false }
}

function Open-CredentialStore([String] $CredentialsFile, [String] $UserName = $null) {
<#
    .SYNOPSIS
        Opens a credentials file and reads the entries.
        
        If a user name is specified, it will look for that entry only. It short circuits if found.
        If a specified user is not found an empty hashmap is returned.

        Results are returned as a hashmap.
    
    .EXAMPLE
        Open-CredentialStore "C:\path\to\key\file"

        Loads all users in the credential file.

    .EXAMPLE
        Open-CredentialStore "C:\path\to\key\file" -UserName "user"

        Loads the specified user only.
#>
    $Local:credentials = @{}

    # The file should exist before trying to read it.
    if([System.IO.File]::Exists($CredentialsFile)) {

        $credentialReader = [System.IO.File]::OpenText($CredentialsFile)
        # Credentials are stored one per line as 'username|password'.
        while($null -ne ($line = $credentialReader.ReadLine())) {
            # Split the line into username and secure string
            $cred = $line.trim().split("|",2)

            if($cred.Count -eq 2 -and (!$UserName -or $cred[0] -eq $UserName))
            {
                $Local:credentials.Add($cred[0], $cred[1])
            }

            # Look no further if a username is specified and the match is found.
            if($cred[0] -eq $UserName) { Break }
        }
        # Cleanup for good measure.
        $credentialReader.Dispose()
        $credentialReader = $null
    } else {
        Write-Information -MessageData ("File not found: {0}." -f $CredentialsFile) -InformationAction Continue
    }
    
    return $Local:credentials
}
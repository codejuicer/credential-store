# Credential Store
This is a rudimentary PowerShell module to create (and read from) secure credential stores.  
It uses WMI, so the stores are not portable. They are specific to a user account and computer \([https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/get-credential?view=powershell-6]\).  
It works with PSCredential objects.

# Installation
Create a directory named CredentialStore under C:\Users\<user name>\Documents\WindowsPowerShell\ and place CredentialStore.psm1 in it.

# Usage
Depending on your PowerShell version, you might need to call `Import-Module CredentialStore`. I think version 3+ do that automatically.

## Creating or adding to a store
`Set-CredentialStoreCredential <path to file> [-UserName <username>]`  
If the file doesn't exist, it will be created. If -UserName is included, the user name field will be pre-populated.

## Retrieving a PSCredential
`Get-CredentialStoreCredential -CredentialFile <path to file> -UserName <username>`
This will give you a PSCredential if the user exists in the file.  
Hint: if you just want the password use `(Get-CredentialStoreCredential <path to file> <username>).GetNetworkCredential().Password`.
